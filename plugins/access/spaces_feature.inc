<?php

/**
 * @file
 * Provides access control for ctools by checking against the current space's
 * feature settings. Optionally allows a permission to be checked in addition
 * to the first feature access control check.
 *
 * This access plugin delegates access control first to spaces_access_feature()
 * which will check whether the user can first access content, then the active
 * space type's 'feature_access' method will be called.  If implementing your
 * own space type you have the ability to define your own access control.  See
 * 'space_og' class in Spaces OG module for an example implementation.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Spaces: access feature"),
  'description' => t('Access will be granted if the given feature is enabled in the current space.'),
  'callback' => 'spaces_extra_spaces_feature_ctools_access_check',
  'settings form' => 'spaces_extra_spaces_feature_ctools_access_settings',
  'summary' => 'spaces_extra_spaces_feature_ctools_access_summary',
);

/**
 * Settings form
 */
function spaces_extra_spaces_feature_ctools_access_settings(&$form, &$form_state, $conf) {
  // Generate feature options.
  foreach (spaces_features() as $feature) {
    $options[$feature->name] = check_plain($feature->info['name']);
  }
  // Select which feature the space must have enabled
  $form['settings']['spaces_feature'] = array(
    '#type' => 'select',
    '#title' => t('Feature'),
    '#default_value' => $conf['spaces_feature'],
    '#options' => $options,
    '#description' => t('Only allow access to this view if the user has access to the selected feature.'),
  );
}

/**
 * Check for access.
 */
function spaces_extra_spaces_feature_ctools_access_check($conf, $context) {
  return spaces_access_feature('view', $conf['spaces_feature'], NULL);
}

/**
 * Provide a summary description based upon the settings.
 */
function spaces_extra_spaces_feature_ctools_access_summary($conf, $context) {
  $features = spaces_features();
  if (isset($features[$conf['spaces_feature']])) {
    return t('Feature: @feature', array('@feature' => $features[$conf['spaces_feature']]->info['name']));
  }
  return t('Broken');
}
