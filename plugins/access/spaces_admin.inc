<?php

/**
 * @file
 * Provides access control for ctools by checking if a user has
 * admin access to the current space.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Spaces: access admin"),
  'description' => t('Access will be granted if the user has admin access to the current space.'),
  'callback' => 'spaces_extra_spaces_admin_ctools_access_check',
  'summary' => 'spaces_extra_spaces_admin_ctools_access_summary',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Check for access.
 */
function spaces_extra_spaces_admin_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  return spaces_access_admin($context->data);
}

/**
 * Provide a summary description based upon the checked roles.
 */
function spaces_extra_spaces_admin_ctools_access_summary($conf, $context) {
  return t('@identifier can administer the current space', array('@identifier' => $context->identifier));
}
